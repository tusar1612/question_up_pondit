<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question extends Model
{
    protected $fillable=['cat_id','op_A','op_B','op_C','op_D','correct_ans','question'];


    public function category(){

        return $this->belongsTo('App\Category');
    }

    public function categoryWise(){

        $question= DB::table('questions')
            ->join('categories', 'questions.id', '=', 'categories.id')

            ->select('questions.*', 'categories.name')
            ->get();
        
        return $question;

    }
    
    
}
