<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {

        $table->increments('id');
        $table->integer('cat_id')->unsigned();
        $table->string('question');
        $table->string('op_A');
        $table->string('op_B');
        $table->string('op_C');
        $table->string('op_D');
        $table->string('correct_ans');
        $table->timestamps();

        $table->foreign('cat_id')->references('id')->on('categories');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
