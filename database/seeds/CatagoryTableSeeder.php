<?php

use Illuminate\Database\Seeder;

class CatagoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' =>'PHP'
        ]);

        DB::table('categories')->insert([
            'name' =>'Laravel'
        ]);

        DB::table('categories')->insert([
            'name' =>'Js'
        ]);

        DB::table('categories')->insert([
            'name' =>'HTML'
        ]);

        DB::table('categories')->insert([
            'name' =>'CSS'
        ]);

    }
}
