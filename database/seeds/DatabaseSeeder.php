<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CatagoryTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
