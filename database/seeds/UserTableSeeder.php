<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->delete();
        \App\User::create(array(
            'name'     => 'admin',
            'email'    => 'rod1612t@gmail.com',
            'password' => Hash::make('admin')
        ));
    }
}
