


@extends('layouts.app')
@section('content')
    <form class="form-basic" method="post" action="/category/{{$cat_id_name->id}}">


        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input name="_method" value="PATCH" type="hidden">

        <div class="form-row">
            <label>
                <span>Add Category</span>
                <input type="text" name="cat_name" value="{{$cat_id_name->name}}">
            </label>
        </div>

        <div class="form-row">
            <button type="submit">Update Category</button>
        </div>

        {{csrf_field()}}

    </form>
@endsection
