<?php
$category=new \App\Category();
$alldata=$category->all();

$sl=1;



?>
@extends('layouts.app')


@section('content')
    <div class="form-basic">
        <div id="mnu">
            <a href="/category/create"><button class="btn btn-warning editable">Create New</button></a>
            <a href="/"><button class="btn btn-warning editable">Home</button></a>

        </div>

    <div class="form-row">

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Sl</th>
                <th width="50%">Category Name</th>
                <th width="30%">Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($alldata as $item)
                <tr>

                    <td>{{$sl++}}</td>
                    <td>{{$item->name}}</td>
                    <td><a href="/category/{{$item->id}}/edit"><button class="btn btn-warning editable" >Edit</button></a>

                        <form action="/category/{{ $item->id}}" method="POST" id="frm">
                            <input type="hidden" name="_method" value="DELETE">

                            <button class="btn btn-warning editable" type="submit" title="Delete" onclick="return confirm('are you want to delete?')">Delete</button>
                            <?php echo csrf_field() ?>
                        </form>



                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>


    </div>
</div>


@endsection