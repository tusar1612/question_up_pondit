<?php

$catagory=new \App\Category();
$alldata=$catagory->all();

//dd($alldata);

?>
@extends('layouts.app')
@section('content')
    <div class="form-row">
        <div id="mnu">
            <a href="/"><button class="btn btn-warning editable">Home</button></a>
        </div>
    </div>
    <form class="form-basic" method="post" action="/question">


        <div class="form-row">
            <label>
                <span>Question Catagory</span>
                <select name="cat_id">
                    <option>Select One</option>

                    @foreach ($alldata as $data) ?>
                    <option value="{{ $data->id}}">{{$data->name}}</option>

                    @endforeach
                </select>
            </label>
        </div>

        <div class="form-row">
            <label>
                <span>Question</span>
                <input type="text" name="question">
            </label>
        </div>
        <div class="form-row">
            <label>
                <span>A</span>
                <input type="text" name="a">
            </label>
        </div>
        <div class="form-row">
            <label>
                <span>B</span>
                <input type="text" name="b">
            </label>
        </div>
        <div class="form-row">
            <label>
                <span>C</span>
                <input type="text" name="c">
            </label>
        </div>
        <div class="form-row">
            <label>
                <span>D</span>
                <input type="text" name="d">
            </label>
        </div>

        <div class="form-row">
            <label><span>Correct Answer</span></label>
            <div class="form-radio-buttons">

                <div>
                    <label>
                        <input type="radio" name="radio" value="A">
                        <span>A</span>
                    </label>
                </div>

                <div>
                    <label>
                        <input type="radio" name="radio" value="B">
                        <span>B</span>
                    </label>
                </div>

                <div>
                    <label>
                        <input type="radio" name="radio" value="C">
                        <span>C</span>
                    </label>
                </div>
                <div>
                    <label>
                        <input type="radio" name="radio" value="D">
                        <span>D</span>
                    </label>
                </div>

            </div>
        </div>

        <div class="form-row">
            <button type="submit">Submit Question</button>
        </div>

        {{csrf_field()}}

    </form>
@endsection


