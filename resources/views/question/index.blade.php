<?php

$question=new \App\Question();

       /* $datas=$question->categoryWise();
        dd($datas);*/

$alldata=$question->categoryWise()
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <title>Question</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Question</h2>
    <div class="form-row">
        <div id="mnu">
            <a href="/question/create"><button class="btn btn-warning editable">Create New</button></a>
            <a href="/"><button class="btn btn-warning editable">Home</button></a>
            <a href="/questions/xml"><button class="btn btn-warning editable">Download Xml file</button></a>

        </div>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Sl</th>
            <th>Catagory</th>
            <th>Question</th>
            <th>option A</th>
            <th>option B</th>
            <th>option C</th>
            <th>option D</th>
            <th>Correct Answer</th>


        </tr>
        </thead>
        <tbody>
        <?php
        $sl=1;
        foreach ($alldata as $data){?>
        <tr>
            <td><?php echo $sl++?></td>
            <td><?php echo $data->name?></td>
            <td><?php echo $data->question?></td>
            <td><?php echo $data->op_A?></td>
            <td><?php echo $data->op_B?></td>
            <td><?php echo $data->op_C?></td>
            <td><?php echo $data->op_D?></td>
            <td><?php echo $data->correct_ans?></td>


        </tr>

        <?php }?>

        </tbody>
    </table>
</div>

</body>
</html>

