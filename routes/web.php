<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Http\Response;






Route::get('/', function () {
    return view('welcome');
});

Route::resource('/question','QuestionController');


Route::resource('/category','CategoryController');

//Route::resource('/category/{id}','CategoryController@edit');









// route to show the login form
/*Route::get('login', array('uses' => 'HomeController@showLogin'));

// route to process the form
Route::post('login', array('uses' => 'HomeController@doLogin'));*/

/*Auth::routes();

Route::get('/', 'HomeController@index');*/
Route::get('/questions/xml', function() {

     $users = \App\Question::all();

     $xml = new XMLWriter();
    //$xml->openURI('test.xml');
    $xml->openUri('tut.xml');
     $xml->openMemory();
     $xml->startDocument();
     $xml->startElement('quiz');

     foreach($users as $user) {


         $xml->startElement('question');
         $xml->writeAttribute('type','multichoice');
         $xml->startElement('name');
            $xml->startElement('text');
                    $xml->writeRaw('Ex-DBR11');
            $xml->endElement();
         $xml->endElement();

         $xml->startElement('questiontext');
            $xml->writeAttribute('format','html');
            $xml->startElement('text');
            $xml->writeCdata( $user->question);
            $xml->endElement();
         $xml->endElement();

         $xml->startElement('generalfeedback');
            $xml->writeAttribute('format','html');
            $xml->startElement('text');
                $xml->writeCdata("check again");
            $xml->endElement();
         $xml->endElement();
         $xml->startElement('defaultgrade');
            $xml->writeRaw('1.0000000');
         $xml->endElement();

         $xml->startElement('penalty');
         $xml->writeRaw('0.3333333');
         $xml->endElement();

         $xml->startElement('hidden');
         $xml->writeRaw('0');
         $xml->endElement();

         $xml->startElement('single');
         $xml->writeRaw('true');
         $xml->endElement();

         $xml->startElement('shuffleanswers');
         $xml->writeRaw('true');
         $xml->endElement();

         $xml->startElement('answernumbering');
         $xml->writeRaw('abc');
         $xml->endElement();

         $xml->startElement('correctfeedback');
            $xml->writeAttribute('format','html');
            $xml->startElement('text');
                $xml->writeRaw('Your answer is correct.');
             $xml->endElement();
         $xml->endElement();

         $xml->startElement('partiallycorrectfeedback');
         $xml->writeAttribute('format','html');
         $xml->startElement('text');
         $xml->writeRaw('Your answer is partially correct.');
         $xml->endElement();
         $xml->endElement();

         $xml->startElement('incorrectfeedback');
         $xml->writeAttribute('format','html');
         $xml->startElement('text');
         $xml->writeRaw('Your answer is incorrect.');
         $xml->endElement();
         $xml->endElement();

         $xml->startElement('shownumcorrect');
         $xml->endElement();

         $xml->startElement('answer');
            $xml->writeAttribute('fraction',"100");
            $xml->writeAttribute('format',"html");
            $xml->startElement('text');
                $xml->writeCdata($user->correct_ans);
            $xml->endElement();
            $xml->startElement('feedback');
                $xml->writeAttribute('format',"html");
                $xml->startElement('text');
                $xml->endElement();
            $xml->endElement();
         $xml->endElement();


         $xml->startElement('answer');
         $xml->writeAttribute('fraction',"0");
         $xml->writeAttribute('format',"html");
         $xml->startElement('text');
         $xml->writeCdata($user->op_B);
         $xml->endElement();
         $xml->startElement('feedback');
         $xml->writeAttribute('format',"html");
         $xml->startElement('text');
         $xml->endElement();
         $xml->endElement();
         $xml->endElement();

         $xml->startElement('answer');
         $xml->writeAttribute('fraction',"0");
         $xml->writeAttribute('format',"html");
         $xml->startElement('text');
         $xml->writeCdata($user->op_C);
         $xml->endElement();
         $xml->startElement('feedback');
         $xml->writeAttribute('format',"html");
         $xml->startElement('text');
         $xml->endElement();
         $xml->endElement();
         $xml->endElement();

         $xml->startElement('answer');
         $xml->writeAttribute('fraction',"0");
         $xml->writeAttribute('format',"html");
         $xml->startElement('text');
         $xml->writeCdata($user->op_D);
         $xml->endElement();
         $xml->startElement('feedback');
         $xml->writeAttribute('format',"html");
         $xml->startElement('text');
         $xml->endElement();
         $xml->endElement();
         $xml->endElement();


         $xml->endElement();








     }

     $xml->endElement();
     $xml->endDocument();

    //$xml->flush();

    //unset($xml);
    $filename='tut.xml';

     $content = $xml->outputMemory();

    file_put_contents($filename,$content);

    $response=new Response($content);

    $response->header('Cache-Control', 'public');
    $response->header('Content-Description', 'File Transfer');
    $response->header('Content-Disposition', 'attachment; filename=tut.xml');
    $response->header('Content-Transfer-Encoding', 'binary');
    return $response->header('Content-Type', 'tut/xml');

    // $xml = null;

  // return response($content)->header('Content-Type', 'tut/xml');
   // return Response::download($content, 'test.xml');

});
